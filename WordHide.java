//*****************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Nick Schake and Tyler Smith
// COMPSC 111 Spring 2016
// Lab # 5
// Date: 2-23-2016
//
// Purpose: To hide a user-chosen word in a 20x20 grid of letters, in order to maximize the inability to find the word. The word is spelled out starting at the upper right hand corner and goes diagonally back to the left hand side, skipping a line each letter and moving over two space. 
//*****************************************
import java.util.Date;
import java.util.Scanner;
import java.util.*;
import java.util.Random;


public class WordHide
{
	public static void main(String[] args)
	{
	Scanner scan = new Scanner(System.in);
	String UserInput;
	String phrase = "UserInput";
	
	//Scanner asking for word
	System.out.println("Input a word with no more than ten characters");
	UserInput = scan.nextLine();
	String CapUserInput;
	String CutUserInput;
	
	CutUserInput = UserInput.substring(0,10);
	CapUserInput = CutUserInput.toUpperCase();
		
	
	String Letter1;
	Letter1 = CapUserInput.substring(0, 1);
	String Letter2;
	Letter2 = CapUserInput.substring(1, 2);
	String Letter3;
	Letter3 = CapUserInput.substring(2, 3);
	String Letter4;
	Letter4 = CapUserInput.substring(3, 4);
	String Letter5;
	Letter5 = CapUserInput.substring(4, 5);
	String Letter6;
	Letter6 = CapUserInput.substring(5, 6);
	String Letter7;
	Letter7 = CapUserInput.substring(6, 7);
	String Letter8;
	Letter8 = CapUserInput.substring(7, 8);
	String Letter9;
	Letter9 = CapUserInput.substring(8, 9);
	String Letter10;
	Letter10 = CapUserInput.substring(9, 10);

	//Creating random letters
	Random r = new Random();

	String alphabet = "abcdefghijklmnopqrstuvwxyz";

	String uncappool = "";
	for (int i = 0; i < 400; i++) {
		uncappool += alphabet.charAt(r.nextInt(alphabet.length()));
		}

	String pool;
	pool = uncappool.toUpperCase();
	
	//Random character strings
	String Line1;
	String Line2;
	String Line3part1;
	String Line3part2;
	String Line4;
	String Line5part1;
	String Line5part2;
	String Line6;
	String Line7part1;
	String Line7part2;
	String Line8;
	String Line9part1;
	String Line9part2;
	String Line10;
	String Line11part1;
	String Line11part2;
	String Line12;
	String Line13part1;
	String Line13part2;
	String Line14;
	String Line15part1;
	String Line15part2;
	String Line16;
	String Line17part1;
	String Line17part2;
	String Line18;
	String Line19part1;
	String Line19part2;
	String Line20;


	//Random character assignments
	Line1 = pool.substring(0, 19);
	Line2 = pool.substring(20, 40);
	Line3part1 = pool.substring(41, 58);
	Line3part2 = pool.substring(58, 60);
	Line4 = pool.substring(60, 80);
	Line5part1 = pool.substring(81, 96);
	Line5part2 = pool.substring(96, 100);
	Line6 = pool.substring(100, 120);
	Line7part1 = pool.substring(121, 134);
	Line7part2 = pool.substring(134, 140);
	Line8 = pool.substring(140, 160);
	Line9part1 = pool.substring(161, 172);
	Line9part2 = pool.substring(172, 180);
	Line10 = pool.substring(180, 200);
	Line11part1 = pool.substring(201, 210);
	Line11part2 = pool.substring(210, 220);
	Line12 = pool.substring(220, 240);
	Line13part1 = pool.substring(241, 248);
	Line13part2 = pool.substring(248, 260);
	Line14 = pool.substring(260, 280);
	Line15part1 = pool.substring(281, 286);
	Line15part2 = pool.substring(286, 300);
	Line16 = pool.substring(300, 320);
	Line17part1 = pool.substring(321, 324);
	Line17part2 = pool.substring(324, 340);
	Line18 = pool.substring(340, 360);
	Line19part1 = pool.substring(361, 362);
	Line19part2 = pool.substring(362, 380);
	Line20 = pool.substring(380, 400);

	//System Outputs
	System.out.println(Line1 + Letter1);
	System.out.println(Line2);
	System.out.println(Line3part1 + Letter2 + Line3part2);
	System.out.println(Line4);
	System.out.println(Line5part1 + Letter3 + Line5part2);
	System.out.println(Line6);
	System.out.println(Line7part1 + Letter4 + Line7part2);
	System.out.println(Line8);
	System.out.println(Line9part1 + Letter5 + Line9part2);
	System.out.println(Line10);
	System.out.println(Line11part1 + Letter6 + Line11part2);
	System.out.println(Line12);
	System.out.println(Line13part1 + Letter7 + Line13part2);
	System.out.println(Line14);
	System.out.println(Line15part1 + Letter8 + Line15part2);
	System.out.println(Line16);
	System.out.println(Line17part1 + Letter9 + Line17part2);
	System.out.println(Line18);
	System.out.println(Line19part1 + Letter10 + Line19part2);
	System.out.println(Line20);
	






	}

}	
